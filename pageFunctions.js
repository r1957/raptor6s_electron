function setFunctionButton(button, label, funct) {
	var but = document.getElementById(button);
	but.innerHTML = "<p>" + label + "</p>";
	but.onclick = funct;
}

function setMainView(view) {
	document.querySelectorAll(".view").forEach((view_element) => {
		view_element.style.visibility = "hidden";
	});
	document.getElementById(view).style.visibility = "visible";
	views[view]()
}
