const {
	app,
	BrowserWindow
} = require('electron');
const globalShortcut = require('electron').globalShortcut
const SerialPort = require('serialport')
const Readline = require('@serialport/parser-readline')

const serialport = new SerialPort('/dev/serial0', {
    baudRate: 115200
})
serialportParser = new Readline()
serialport.pipe(serialportParser)

const TouchscreenWindow = require('electron-touchscreen');

app.on('ready', function() {
	globalShortcut.register('f5', function() {
		// console.log('f5 is pressed')
		mainWindow.reload()
	})
	globalShortcut.register('f12', function() {
		// console.log('f12 is pressed')
		mainWindow.toggleDevTools()
	})
	serialportParser.on('data', line => {
		console.log(line);
	})
	var mainWindow = new TouchscreenWindow({
	// var mainWindow = new BrowserWindow({
		show: false,
	});
	// mainWindow.webContents.openDevTools()

	mainWindow.loadFile('index.html');
	mainWindow.setBackgroundColor('#000000')

	mainWindow.maximize();
	mainWindow.setKiosk(true);
	mainWindow.show();
});
