function hardwareStatusView() {
	console.log("hardwareStatusView");

	document.querySelectorAll(".button-value").forEach((button_value) => {
		button_value.onclick = () => {
			button_value.classList.toggle("button-led-on");
		}
	});

	setFunctionButton("button_F1", "Test Char. LCD", () => {
		console.log("Test character LCD");
	})
	setFunctionButton("button_F2", "Test Round LCD", () => {
		console.log("Test round LCD");
	})
	setFunctionButton("button_F3", "Test Pixels Left", () => {
		console.log("Test Pixels Left");
	})
	setFunctionButton("button_F4", "Test Pixels Right", () => {
		console.log("Test Pixels Right");
	})
	setFunctionButton("button_F5", "Test Button LED", () => {
		console.log("Test Button LED");
	})
	setFunctionButton("button_F6", "Play Boot sequence", () => {
		console.log("Play Boot sequence");
	})

}
